package com.example.proyect.API.Services;

import com.example.proyect.POJO.Instalacio;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface InstalacioService {
    @GET("api/instalacions")
    Call<ArrayList<Instalacio>> getInstalacions();

    @GET("api/instalacions/{id}")
    Call<Instalacio> getInstalacio(@Path("id") int id);

    @POST("api/instalacions")
    Call<Instalacio> newInstalacio(@Body Instalacio instalacio);

    @DELETE("api/instalacions/{id}")
    Call<Instalacio> deleteInstalacio(@Path("id") int id);

    @PUT("api/instalacions/{id}")
    Call<ResponseBody> updateInstalacio(@Path("id") int id, @Body Instalacio instalacio);
}