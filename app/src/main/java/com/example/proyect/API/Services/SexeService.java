package com.example.proyect.API.Services;

import com.example.proyect.POJO.Instalacio;
import com.example.proyect.POJO.Sexe;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface SexeService {
    @GET("api/sexes")
    Call<ArrayList<Sexe>> getSexes();

    @GET("api/sexes/{id}")
    Call<Sexe> getSexe(@Path("id") int id);

    @POST("api/sexes")
    Call<Sexe> newSexe(@Body Sexe sexe);

    @DELETE("api/sexes/{id}")
    Call<Sexe> deleteSexe(@Path("id") int id);

    @PUT("api/sexes/{id}")
    Call<ResponseBody> updateSexe(@Path("id") int id, @Body Sexe sexe);
}