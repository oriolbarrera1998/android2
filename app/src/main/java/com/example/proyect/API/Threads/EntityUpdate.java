package com.example.proyect.API.Threads;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EntitatService;
import com.example.proyect.POJO.Entitat;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class EntityUpdate extends Thread {
    private Entitat entitat;
    private Response<ResponseBody> response;

    public EntityUpdate(Entitat entitat) {
        this.entitat = entitat;
    }

    public Response<ResponseBody> getResponse() {
        return response;
    }

    @Override
    public void run() {
        EntitatService service = API.getApi().create(EntitatService.class);

        Call<ResponseBody> updateEntitat = service.updateEntitat(entitat.getId(), entitat);

        try {
            response = updateEntitat.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
