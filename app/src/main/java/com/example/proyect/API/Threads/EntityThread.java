package com.example.proyect.API.Threads;

import android.app.Activity;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EntitatService;
import com.example.proyect.POJO.Entitat;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class EntityThread extends Thread {

    private int id;
    private Entitat entitat;


    public EntityThread(int id) {

        this.id = id;
    }


    public void run() {
        getEntitatFormBD();
    }

    public Entitat getEntitat() {
        return entitat;
    }

    private void getEntitatFormBD(){
        EntitatService eService = API.getApi().create(EntitatService.class);

        Call<Entitat> ent = eService.getEntitat(this.id);

        try {
            Response<Entitat> response = ent.execute();
            if (response.isSuccessful()) {
                if(response.body() != null){
                    this.entitat = response.body();
                }else{
                    this.entitat = null;
                }

            } else {
                this.entitat = null;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
