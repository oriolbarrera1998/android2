package com.example.proyect.API.Services;

import com.example.proyect.POJO.Dies_setmana;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DiesSetmanaService {
    @GET("api/dies_setmana")
    Call<ArrayList<Dies_setmana>> getDies();
}
