package com.example.proyect.API.Services;

import com.example.proyect.POJO.Equip;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface EquipService {
    @GET("api/equips")
    Call<ArrayList<Equip>> getEquips();

    @GET("api/equips/{id}")
    Call<Equip> getEquip(@Path("id") int id);

    @POST("api/equips")
    Call<Equip> newEquip(@Body Equip equip);

    @DELETE("api/equips/{id}")
    Call<Equip> deleteEquip(@Path("id") int id);

    @PUT("api/equips/{id}")
    Call<ResponseBody> updateEquip(@Path("id") int id, @Body Equip equip);
}