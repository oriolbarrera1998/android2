package com.example.proyect.API.Serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class BooleanSerializer implements JsonSerializer<Boolean> {
    @Override
    public JsonElement serialize(Boolean src, Type typeOfSrc, JsonSerializationContext context) {
        JsonElement element;
        if(src)
            element = new JsonPrimitive(1);
        else
            element = new JsonPrimitive(0);
        return element;
    }
}
