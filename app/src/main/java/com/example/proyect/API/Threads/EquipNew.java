package com.example.proyect.API.Threads;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EquipService;
import com.example.proyect.POJO.Equip;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class EquipNew extends Thread {
    private Equip equip;
    private Response<Equip> response;

    public EquipNew(Equip equip) {
        this.equip = equip;
    }

    public Response<Equip> getResponse() {
        return response;
    }

    @Override
    public void run() {
        EquipService service = API.getApi().create(EquipService.class);

        Call<Equip> newEquip = service.newEquip(equip);

        try {
            response = newEquip.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}