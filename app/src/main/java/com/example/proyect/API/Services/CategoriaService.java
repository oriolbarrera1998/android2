package com.example.proyect.API.Services;

import com.example.proyect.POJO.Categoria;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CategoriaService {
    @GET("api/categories")
    Call<ArrayList<Categoria>> getCategories();

    @GET("api/categories/{id}")
    Call<Categoria> getCategoria(@Path("id") int id);

    @POST("api/categories")
    Call<Categoria> newCategoria(@Body Categoria categoria);

    @DELETE("api/categories/{id}")
    Call<Categoria> deleteCategoria(@Path("id") int id);

    @PUT("api/categories/{id}")
    Call<ResponseBody> updateCategoria(@Path("id") int id, @Body Categoria categoria);
}