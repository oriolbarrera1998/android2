package com.example.proyect.API.Serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.SimpleDateFormat;

public class TimeSerializer implements JsonSerializer<Time> {
    private static final String TIME_FORMAT = "HH:mm:ss";

    @Override
    public JsonElement serialize(Time src, Type typeOfSrc, JsonSerializationContext context) {
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
        JsonElement element;
        element = new JsonPrimitive(sdf.format(src));
        return element;
    }
}
