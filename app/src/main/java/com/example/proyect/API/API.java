package com.example.proyect.API;

import com.example.proyect.API.Deserializers.BooleanDeserializer;
import com.example.proyect.API.Deserializers.TimeDeserializer;
import com.example.proyect.API.Serializers.BooleanSerializer;
import com.example.proyect.API.Serializers.TimeSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.sql.Time;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    private static final String BASE_URL = "http://www.abp-politecnics.com/2019/dam/projecte02/dt03/";
    // private static final String BASE_URL = "http://10.0.2.2/"; // Localhost
    // private static final String BASE_URL = "http://88.19.178.73/"; // IP Marc
    // private static final String BASE_URL = "http://192.168.10.49/"; // IP Marc CEP

    private static Retrofit retrofit = null;

    public static Retrofit getApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("api-key", "3756a179fb9596303c80").build();
                return chain.proceed(request);
            }
        });

        if (retrofit == null) {
            OkHttpClient client = httpClient.build();

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Time.class, new TimeDeserializer());
            gsonBuilder.registerTypeAdapter(Time.class, new TimeSerializer());
            gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanDeserializer());
            gsonBuilder.registerTypeAdapter(boolean.class, new BooleanDeserializer());
            gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanSerializer());
            gsonBuilder.registerTypeAdapter(boolean.class, new BooleanSerializer());
            Gson gSon = gsonBuilder.create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gSon))
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
