package com.example.proyect.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.proyect.POJO.Dies_setmana;
import com.example.proyect.POJO.Horaris_adients;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;

import java.sql.Time;
import java.util.ArrayList;

public class AdapterNovesDemandes extends RecyclerView.Adapter<AdapterNovesDemandes.myViewHolder> {

    ArrayList<Horaris_adients> listDemandes;
    ArrayList<Dies_setmana> dies;

    private OnItemClick myCallback;

    public interface OnItemClick {
        void onClick(Horaris_adients horari, int position);
    }

    public AdapterNovesDemandes(ArrayList<Horaris_adients> passedListItem, OnItemClick myCallBack) {
        this.listDemandes = passedListItem;

        dies = Preferences.getDiesSetmana();
        this.myCallback = myCallBack;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.novademandaitem, parent, false);

        myViewHolder holder = new myViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        final Dies_setmana[] diaSetmana = new Dies_setmana[1];

        holder.numberPickerDurada.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                diaSetmana[0] = dies.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.numberPickerHoraInici.getValue() != holder.numberPickerHoraFinal.getValue()) {
                    Horaris_adients time = new Horaris_adients(diaSetmana[0].getId(), new Time(holder.numberPickerHoraInici.getValue(), 00, 00), new Time(holder.numberPickerHoraFinal.getValue(), 00, 00));
                    listDemandes.set(position, time);
                    myCallback.onClick(listDemandes.get(position), position);
                    holder.select.setChecked(true);
                } else {
                    Toast.makeText(v.getContext(), "La hora de inici i de fi no poden ser iguals", Toast.LENGTH_SHORT).show();
                    holder.select.setChecked(false);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return listDemandes.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        Spinner numberPickerDurada;
        NumberPicker numberPickerHoraFinal;
        NumberPicker numberPickerHoraInici;
        Switch select;
        CardView cardView;

        public myViewHolder(View view) {
            super(view);
            numberPickerDurada = view.findViewById(R.id.comboBoxDies);
            numberPickerHoraFinal = view.findViewById(R.id.timePickerFinal);
            numberPickerHoraInici = view.findViewById(R.id.timePickerInici);
            cardView = view.findViewById(R.id.cardviewact);
            select = view.findViewById(R.id.seleccionar);
            numberPickerHoraFinal.setMaxValue(23);
            numberPickerHoraFinal.setMinValue(0);
            numberPickerHoraInici.setMaxValue(23);
            numberPickerHoraInici.setMinValue(0);

            ArrayList<String> diesNoms = new ArrayList<>();

            for (Dies_setmana d : dies) {
                diesNoms.add(d.getDia());
            }

            ArrayAdapter<String> spinnerDies = new ArrayAdapter(
                    view.getContext(),
                    android.R.layout.simple_spinner_item,
                    diesNoms
            );

            spinnerDies.setDropDownViewResource(R.layout.spinneritemlayout);
            numberPickerDurada.setAdapter(spinnerDies);
            numberPickerDurada.setSelection(0);
            /*
            newListDemandes = new ArrayList<>();
            for (int i = 0; i < listDemandes.size(); i++)
            {
                newListDemandes.get(i).setDurada(numberPickerDurada.getValue());

            }
            myCallback.listenerMethod(newListDemandes);
            */
        }
    }
}