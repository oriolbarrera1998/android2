package com.example.proyect.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.proyect.Fragments.StartDemandFragment;
import com.example.proyect.Activities.InstalacioDetail;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;

import java.util.ArrayList;
import java.util.List;

public class InstalacioAdapter extends RecyclerView.Adapter<InstalacioAdapter.ViewHolder> {


    private List<Instalacio> instalacions;
    private Context mContext;


    // Constructor
    public InstalacioAdapter(Context context, ArrayList<Instalacio> instalacions) {
        this.instalacions = instalacions;
        this.mContext = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_demand, viewGroup, false);
        ViewHolder vHolder = new ViewHolder(view);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.name.setText(instalacions.get(i).getNom());
        viewHolder.address.setText(instalacions.get(i).getDireccio());
        viewHolder.img.setImageDrawable(mContext.getResources().getDrawable(mContext.getResources().getIdentifier(instalacions.get(i).getImatge(), "drawable", mContext.getPackageName())));
        viewHolder.parentItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InstalacioDetail.class);
                intent.putExtra(StartDemandFragment.EXTRA_TEXTO, instalacions.get(i));
                mContext.startActivity(intent);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, address;
        private ImageView img;
        private LinearLayout parentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.instalacioName);
            address = itemView.findViewById(R.id.instalacioAddress);
            img = itemView.findViewById(R.id.instalacioImg);
            parentItem = itemView.findViewById(R.id.parent_item);
        }
    }

    @Override
    public int getItemCount() {
        return instalacions.size();
    }
}