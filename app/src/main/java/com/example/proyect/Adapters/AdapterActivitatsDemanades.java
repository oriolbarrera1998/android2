package com.example.proyect.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.proyect.POJO.Activitats_demanades;
import com.example.proyect.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterActivitatsDemanades extends RecyclerView.Adapter<AdapterActivitatsDemanades.ViewHolder> implements View.OnClickListener {
    private View.OnClickListener listener;
    private List<Activitats_demanades> activitats_demanades;
    private Context mContext;


    // Constructor
    public AdapterActivitatsDemanades(Context context, ArrayList<Activitats_demanades> activitats_demanades) {
        this.activitats_demanades = activitats_demanades;
        this.mContext = context;
    }


    @NonNull
    @Override
    public AdapterActivitatsDemanades.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(mContext).inflate(R.layout.item_activitat, viewGroup, false);
        AdapterActivitatsDemanades.ViewHolder vHolder = new AdapterActivitatsDemanades.ViewHolder(view);
        view.setOnClickListener(this);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterActivitatsDemanades.ViewHolder viewHolder, int i) {
        viewHolder.name.setText(activitats_demanades.get(i).getNom());
        viewHolder.address.setText(activitats_demanades.get(i).getTipus());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, address;
        private CardView parentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nomActivitat);
            address = itemView.findViewById(R.id.tipoActivitat);
            parentItem = itemView.findViewById(R.id.cardviewActivity);
        }
    }

    @Override
    public void onClick(View v) {
        if(listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return activitats_demanades.size();
    }
}
