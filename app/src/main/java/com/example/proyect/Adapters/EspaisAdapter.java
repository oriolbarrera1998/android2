package com.example.proyect.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.proyect.POJO.Espai;
import com.example.proyect.R;

import java.util.ArrayList;
import java.util.List;

public class EspaisAdapter extends RecyclerView.Adapter<EspaisAdapter.ViewHolder> implements View.OnClickListener {

private List<Espai> espais;
private View.OnClickListener listener;
private Context mContext;
// Metode del listener

public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
}

// Metode click

@Override
public void onClick(View view) {
    if (listener != null) {
        listener.onClick(view);
    }
}

// Classe ViewHolder

    public static class ViewHolder extends RecyclerView.ViewHolder {
    TextView textViewNom;
    TextView textViewPreu;
    ImageView imageView;
    public ViewHolder(View itemView) {
        super(itemView);

        textViewNom = itemView.findViewById(R.id.espaiNom);
        textViewPreu = itemView.findViewById(R.id.espaiCost);
        imageView = itemView.findViewById(R.id.espaiImage);

    }

    // Metode bindPlace omple els camps al Recycler View

    public void bindPlace(Espai p) {
        textViewNom.setText(p.getNom());
        textViewPreu.setText(String.format("%.2f €", p.getPreu()));
    }
}

    // Metode constructor

    public EspaisAdapter(ArrayList<Espai> espais, Context mContext) {
        this.espais = espais;
        this.mContext = mContext;
    }


    // Metode onCreateViewHolder

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_espai, parent, false);
        itemView.setOnClickListener(this);
        ViewHolder vh = new ViewHolder(itemView);

        return vh;
    }

    // Metode onBindViewHolder, per executar el metode que omple els camps del layout

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Espai item = espais.get(position);
        holder.bindPlace(item);
        holder.imageView.setImageDrawable(mContext.getResources().getDrawable(mContext.getResources().getIdentifier(espais.get(position).getImatge(), "drawable", mContext.getPackageName())));

    }

    // Metode getItemCount per saber el tamany de la llista

    @Override
    public int getItemCount() {
        return espais.size();
    }
}
