package com.example.proyect.Exceptions;

public class FailedDataBaseConnectionException extends Exception {
    public static final String MSG = "El servei no esta disponible actualment...";

    public FailedDataBaseConnectionException(){
        super(MSG);
    }
}
