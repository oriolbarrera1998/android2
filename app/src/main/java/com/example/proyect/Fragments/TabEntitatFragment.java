package com.example.proyect.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.InstalacioService;
import com.example.proyect.Activities.EditarEntitat;
import com.example.proyect.Activities.MainActivity;
import com.example.proyect.Activities.PlayVideo;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabEntitatFragment extends Fragment implements OnMapReadyCallback, Serializable {
    Entitat entitat;
    View v;
    TextView correu, direccio, cif, telefon;
    FloatingActionButton add, edit, play;
    ArrayList<Instalacio> instalacions;
    public static final int REQUEST_VIDEO_CAPTURE = 50;
    public static final String VIDEO_PLAY = "adsaa";

    public static final String REQUEST_EDIT_PROFILE = "EDIT_0128";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MainActivity mainActivity = (MainActivity) getActivity();
        v = inflater.inflate(R.layout.fragment_tab_entitat, container, false);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        edit = v.findViewById(R.id.floationButtonEditar);
        add = v.findViewById(R.id.AddVideo);
        play = v.findViewById(R.id.Play);


        if (mainActivity.getMyEntitat() != null) {
            entitat = mainActivity.getMyEntitat();
            omplirTabEntitat();
        }

        if (entitat.getVideo() == null) {
            play.hide();
            add.show();
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    getActivity().startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                }
            });
        } else {
            add.hide();
            play.show();
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent playVideo = new Intent(getActivity(), PlayVideo.class);
                    playVideo.putExtra(VIDEO_PLAY, entitat.getVideo());
                    getActivity().startActivity(playVideo);
                }
            });

        }

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapEntitat);
        mapFragment.getMapAsync(this);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditarEntitat.class);
                i.putExtra(REQUEST_EDIT_PROFILE, entitat);
                getActivity().startActivityForResult(i, MainActivity.REQUEST_EDIT_ENTITAT);
            }
        });
        return v;
    }

    private void omplirTabEntitat() {
        correu = v.findViewById(R.id.textboxCorreo);
        cif = v.findViewById(R.id.textboxCIF);
        direccio = v.findViewById(R.id.textboxDirección);
        telefon = v.findViewById(R.id.textboxTelefono);

        telefon.setText(entitat.getTelefons().get(0).getTelefon());
        correu.setText(entitat.getCorreu());
        direccio.setText(entitat.getDireccio());
        cif.setText(entitat.getCif());
    }

    // Include the OnCreate() method here too, as described above.

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        LatLng sydney = new LatLng(entitat.getLatitud(), entitat.getLongitud());
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        googleMap.addMarker(new MarkerOptions().position(sydney).snippet(entitat.getCorreu())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.iconmarker)).title(entitat.getNom()));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

        InstalacioService serviceInstalacions = API.getApi().create(InstalacioService.class);
        Call<ArrayList<Instalacio>> callInstalacio = serviceInstalacions.getInstalacions();

        callInstalacio.enqueue(new Callback<ArrayList<Instalacio>>() {
            @Override
            public void onResponse(Call<ArrayList<Instalacio>> call, Response<ArrayList<Instalacio>> response) {
                instalacions = response.body();
                ArrayList<String> list = new ArrayList<>();

                for (int i = 0; i < instalacions.size(); i++) {
                    createMarker(instalacions.get(i).getLatitud(), instalacions.get(i).getLongitud(), instalacions.get(i).getNom(), instalacions.get(i).getDireccio(), R.drawable.google_plus, googleMap);
                }

            }

            @Override
            public void onFailure(Call<ArrayList<Instalacio>> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
            }

        });

    }

    private Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID, GoogleMap googleMap) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }


}
