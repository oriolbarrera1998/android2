package com.example.proyect.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.proyect.Activities.EditarEquip;
import com.example.proyect.Activities.MainActivity;
import com.example.proyect.Adapters.AdapterEquips;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.POJO.Equip;
import com.example.proyect.R;

import java.util.ArrayList;

public class TabEquipsFragment extends Fragment {
    private RecyclerView recViewEquips;
    private Entitat entitat;
    FloatingActionButton floatingActionButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_equips, container, false);

        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity.getMyEntitat() != null) {
            entitat = mainActivity.getMyEntitat();
            recViewEquips = view.findViewById(R.id.RecViewEquips);
            ArrayList<Equip> equips = entitat.getEquips();
            AdapterEquips adapter = new AdapterEquips(getContext(), equips);
            recViewEquips.setHasFixedSize(true);
            recViewEquips.setLayoutManager(new GridLayoutManager(getContext(), 1));
            recViewEquips.setAdapter(adapter);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        }

        floatingActionButton = view.findViewById(R.id.floatingAction);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EditarEquip.class);
                i.putExtra(EditarEquip.CREATING_BOOL, true);
                getActivity().startActivityForResult(i, MainActivity.REQUEST_EDIT_EQUIP);
            }
        });

        return view;
    }
}
