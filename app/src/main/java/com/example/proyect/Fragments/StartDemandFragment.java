package com.example.proyect.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.InstalacioService;
import com.example.proyect.Activities.MainActivity;
import com.example.proyect.Adapters.InstalacioAdapter;
import com.example.proyect.POJO.Instalacio;
import com.example.proyect.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StartDemandFragment extends Fragment {
    View v;
    private RecyclerView recViewInstalacions;
    private ArrayList<Instalacio> instalacions;
    public static final String EXTRA_TEXTO = "DEMAND";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_start_demand, container, false);
        InstalacioService service = API.getApi().create(InstalacioService.class);

        Call<ArrayList<Instalacio>> callInstalacions = service.getInstalacions();



        callInstalacions.enqueue(new Callback<ArrayList<Instalacio>>() {
            @Override
            public void onResponse(Call<ArrayList<Instalacio>> call, Response<ArrayList<Instalacio>> response) {
                instalacions = response.body();
                recViewInstalacions = v.findViewById(R.id.RecViewInstalacions);
                InstalacioAdapter adapter = new InstalacioAdapter(getContext(), instalacions);
                recViewInstalacions.setHasFixedSize(true);
                recViewInstalacions.setLayoutManager(new GridLayoutManager(getContext(), 1));
                recViewInstalacions.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<ArrayList<Instalacio>> call, Throwable t) {
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
        return v;
    }

    // En este metodo llamamos al Manager para coger los datos y seguidamente al Adapter para meter estos datos en los items de la GridView.

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

    }
}