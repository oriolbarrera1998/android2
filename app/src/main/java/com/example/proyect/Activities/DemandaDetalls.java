package com.example.proyect.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.ActivitatDemanadaService;
import com.example.proyect.Adapters.HorariAdapter;
import com.example.proyect.POJO.Activitats_demanades;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DemandaDetalls extends AppCompatActivity {
    public static final String ACTIVITAT_DEMANADA = "activitat_demanada";
    private Activitats_demanades activitat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demanda_detalls);

        Entitat entitat = MainActivity.getEntitat();
        activitat = (Activitats_demanades) getIntent().getExtras().get(ACTIVITAT_DEMANADA);

        TextView activitatName = findViewById(R.id.activitatName);
        TextView espai = findViewById(R.id.espaiName);
        TextView instalacio = findViewById(R.id.instalacioName);
        TextView equip = findViewById(R.id.equipName);
        TextView dies = findViewById(R.id.numDies);
        TextView hores = findViewById(R.id.numHores);
        TextView horariTitle = findViewById(R.id.horariTitle);
        TextView tipus = findViewById(R.id.tipus);
        CardView borrar = findViewById(R.id.deleteButton);

        activitatName.setText(activitat.getNom());
        horariTitle.setText("Horaris activitat");
        espai.setText(activitat.getEspais().getNom());
        instalacio.setText(activitat.getEspais().getInstalacions().getNom());
        equip.setText(entitat.getEquipById(activitat.getId_equip()).getNom());
        dies.setText(activitat.getDies() + (activitat.getDies() > 1 ? " Dies" : " Dia"));
        hores.setText(activitat.getDurada() + (activitat.getDurada() > 1 ? " Hores" : " Hora"));
        tipus.setText(activitat.getTipus());

        borrar.setVisibility(View.VISIBLE);

        HorariAdapter adapter = new HorariAdapter(activitat.getHoraris_adients());

        RecyclerView list = findViewById(R.id.listHoraris);
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitatDemanadaService service = API.getApi().create(ActivitatDemanadaService.class);

                Call<Activitats_demanades> call = service.deleteActivitat(activitat.getId());

                call.enqueue(new Callback<Activitats_demanades>() {
                    @Override
                    public void onResponse(Call<Activitats_demanades> call, Response<Activitats_demanades> response) {
                        if(String.valueOf(response.code()).split("")[1].equals("2")) {
                            finish();
                        } else {
                            Toast.makeText(DemandaDetalls.this, "Alguna cosa ha anat malament", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Activitats_demanades> call, Throwable t) {
                        Toast.makeText(DemandaDetalls.this, "No s'ha pogut connectar", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
    }
}
