package com.example.proyect.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.proyect.Interfaces.ActionBar;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;
import com.example.proyect.WelcomeActivity;

public class Splash extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences.setSettings(getSharedPreferences(Preferences.getPrefsName(), 0));
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_splash);


        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                if(Preferences.isFirstTime()){
                    Intent i = new Intent(Splash.this, WelcomeActivity.class);
                    Splash.this.startActivity(i);
                    Preferences.saveFirstTimePreferences();
                    finish();
                }else{
                    if(Preferences.isUserLoggedIn()){
                        Intent i = new Intent(Splash.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }else{
                        Intent i = new Intent(Splash.this, Login.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        }, SPLASH_DISPLAY_LENGTH);


    }

    /** Called when the activity is first created. */
//    @Override
    //  public void onCreate(Bundle icicle) {

}
