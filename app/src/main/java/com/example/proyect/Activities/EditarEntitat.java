package com.example.proyect.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EntitatService;
import com.example.proyect.Fragments.TabEntitatFragment;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.POJO.Telefon;
import com.example.proyect.R;
import com.example.proyect.Utils.StringHashing;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarEntitat extends AppCompatActivity {
    EditText correu, direccio, cif, telefon, nom, passwordantigua, passwordnueva;
    FloatingActionButton done;
    Entitat entitat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_entitat);
        entitat = getIntent().getParcelableExtra(TabEntitatFragment.REQUEST_EDIT_PROFILE);

        // Omplim les dades amb les que té la entitat actual
        omplirCamps();

        done = findViewById(R.id.floationButtonDone);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!passwordantigua.getText().toString().isEmpty()) {
                    ArrayList<Telefon> newTelefon = entitat.getTelefons();
                    newTelefon.get(0).setTelefon(telefon.getText().toString());

                    if (entitat.getPassword().equals(StringHashing.hashString(passwordantigua.getText().toString()))) {
                        actualitzarDades();

                        EntitatService service = API.getApi().create(EntitatService.class);

                        Call<ResponseBody> callUpdate = service.updateEntitat(entitat.getId(), entitat);

                        callUpdate.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.code() == 204) {
                                    Toast.makeText(EditarEntitat.this, "Perfil actualitzat correctament", Toast.LENGTH_SHORT).show();
                                    setResult(RESULT_OK);
                                    finish();
                                } else if (response.code() == 401) {
                                    Toast.makeText(EditarEntitat.this, "Unauthorized (HOSTING)", Toast.LENGTH_SHORT).show();
                                } else {
                                    try {
                                        Toast.makeText(EditarEntitat.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    //Toast.makeText(EditarEntitat.this, "Alguna cosa ha anat malament...", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                // Toast.makeText(EditarEntitat.this, t.toString(), Toast.LENGTH_LONG).show();
                                Toast.makeText(EditarEntitat.this, "No s'ha pogut connectar amb el servidor", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else {
                    passwordantigua.setHighlightColor(getResources().getColor(R.color.red));
                    passwordantigua.requestFocus();
                    passwordantigua.setBackgroundResource(R.drawable.shapes);
                    passwordantigua.setHint("Requerit");
                    passwordantigua.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
        });
    }

    public void omplirCamps() {
        correu = findViewById(R.id.editTextCorreu);
        direccio = findViewById(R.id.editTextLocation);
        cif = findViewById(R.id.editTextCIF);
        passwordantigua = findViewById(R.id.editTextPasswordAntiga);
        passwordnueva = findViewById(R.id.editTextPassword);
        telefon = findViewById(R.id.editTextTelefon);
        nom = findViewById(R.id.editTextNom);

        correu.setText(entitat.getCorreu());
        direccio.setText(entitat.getDireccio());
        cif.setText(entitat.getCif());
        telefon.setText(entitat.getTelefons().get(0).getTelefon());
        nom.setText(entitat.getNom());
    }

    private void actualitzarDades() {
        entitat.setCorreu(correu.getText().toString());
        entitat.setDireccio(direccio.getText().toString());
        entitat.setCif(cif.getText().toString());
        entitat.setNom(nom.getText().toString());
        entitat.setEquips(new ArrayList());
    }
}
