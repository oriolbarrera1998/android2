package com.example.proyect.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EntitatService;
import com.example.proyect.Exceptions.NullEntityException;
import com.example.proyect.Exceptions.UserPasswordException;
import com.example.proyect.Interfaces.ActionBar;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;
import com.example.proyect.Utils.StringHashing;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity {

    boolean canClick;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences.setSettings(getSharedPreferences(Preferences.getPrefsName(), 0));
        Preferences.setTheme(this);
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        if(Preferences.isDarkModeSaved()){
            setContentView(R.layout.activity_login_dark);
        }else{
            setContentView(R.layout.activity_login);
        }



        //Views
        ConstraintLayout mainLoginLayout = findViewById(R.id.mainLoginLayout);
        Button btnLogin = findViewById(R.id.btnLogin);
        ImageView imgLogo = findViewById(R.id.imgLogo);
        ImageButton imgHidePassword = findViewById(R.id.btnShowPassword);
        final EditText txtBoxPassword = findViewById(R.id.txtBoxPassword);
        final EditText txtBoxUser = findViewById(R.id.txtBoxUsername);
        TextView txtContact = findViewById(R.id.txtContacte);

        imgLogo.setImageResource(R.drawable.simbol);
        txtContact.setPaintFlags(txtContact.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        canClick = checkValues(txtBoxPassword, txtBoxUser);

        mainLoginLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                //Quan es fa login, guardar la variable a sharedPreferences

                if(canClick) {
                    login(txtBoxUser.getText().toString(), txtBoxPassword.getText().toString());

                }else{
                    Toast.makeText(getApplicationContext(), "Falta algun camp", Toast.LENGTH_LONG).show();
                }

            }
        });

        txtBoxPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }

            @Override
            public void afterTextChanged(Editable s) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }
        });

        txtBoxUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }

            @Override
            public void afterTextChanged(Editable s) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }
        });



        imgHidePassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    txtBoxPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    txtBoxPassword.setSelection(txtBoxPassword.getText().length());
                    txtBoxPassword.setTypeface(Typeface.DEFAULT);
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    txtBoxPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    txtBoxPassword.setSelection(txtBoxPassword.getText().length());
                    txtBoxPassword.setTypeface(Typeface.DEFAULT);
                }
                return true;
            }
        });
    }

    public boolean checkValues(EditText p, EditText u){
        boolean value;
        String pass = p.getText().toString();
        String user = u.getText().toString();

        return !(pass.isEmpty() || user.isEmpty());
    }

    public void login(String correu, String password){
        final Entitat[] e = new Entitat[1];

        //Create service
        EntitatService eService = API.getApi().create(EntitatService.class);
        Call<Entitat> ent = eService.getEntitatCorreu(correu);

        ent.enqueue(new Callback<Entitat>() {


            @Override
            public void onResponse(Call<Entitat> call, Response<Entitat> response){
                if(response.body() != null){
                    e[0] = response.body();
                    try{
                        if(isPasswordCorrect(password, e[0])){
                            Preferences.saveUser(e[0].getId());
                            loginSuccessfulNavigate(e[0]);
                        }
                    }catch (UserPasswordException ex){
                        Toast.makeText(Login.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(Login.this, new NullEntityException().getMessage(), Toast.LENGTH_LONG).show();
                }

            }


            @Override
            public void onFailure(Call<Entitat> call, Throwable t) {
                Toast.makeText(Login.this, t.toString(), Toast.LENGTH_SHORT).show();
                //Toast.makeText(Login.this, new FailedDataBaseConnectionException().getMessage(), Toast.LENGTH_LONG).show();
            }


        });



    }

    public boolean isPasswordCorrect(String password, Entitat entitat) throws UserPasswordException{
        boolean isPasswordCorrect = false;

        //Encriptacio de la password
        if(entitat.getPassword().equals(StringHashing.hashString(password))){
            isPasswordCorrect = true;
        }else{
            throw  new UserPasswordException();
        }

        return isPasswordCorrect;
    }

    public void hideKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void loginSuccessfulNavigate(Entitat entitat){
        Preferences.saveEntitat(entitat);
        Preferences.setIsEntityRetrieved(true);
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}