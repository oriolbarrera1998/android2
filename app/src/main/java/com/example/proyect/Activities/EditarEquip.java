package com.example.proyect.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.CategoriaService;
import com.example.proyect.API.Services.EquipService;
import com.example.proyect.API.Services.EsportService;
import com.example.proyect.API.Services.SexeService;
import com.example.proyect.POJO.Categoria;
import com.example.proyect.POJO.Equip;
import com.example.proyect.POJO.Esport;
import com.example.proyect.POJO.Sexe;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarEquip extends AppCompatActivity {
    public static final String EXTRA_EQUIP = "EQUIP";
    public static final String CREATING_BOOL = "creatingBool";
    private boolean creatingTeam;

    Spinner categoriaSpinner, esportSpinner, sexeSpinner;
    TextView nom;
    FloatingActionButton floatingActionButton;
    Equip equip;
    ArrayList<Categoria> categories;
    ArrayList<Esport> esports;
    ArrayList<Sexe> sexes;
    private int selectedCategoria;
    private int selectedSexe;
    private int selectedEsport;

    private EquipService serviceEquip = API.getApi().create(EquipService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_equip);
        equip = new Equip();

        creatingTeam = getIntent().getBooleanExtra(CREATING_BOOL, true);

        if (getIntent().getParcelableExtra(EXTRA_EQUIP) != null) {
            equip = getIntent().getParcelableExtra(EXTRA_EQUIP);
        }

        EsportService serviceEsport = API.getApi().create(EsportService.class);
        Call<ArrayList<Esport>> callEsports = serviceEsport.getEsports();

        CategoriaService serviceCategoria = API.getApi().create(CategoriaService.class);
        Call<ArrayList<Categoria>> callCategoria = serviceCategoria.getCategories();

        SexeService serviceSexe = API.getApi().create(SexeService.class);
        Call<ArrayList<Sexe>> callSexe = serviceSexe.getSexes();
        OmplirPantalla();
        callEsports.enqueue(new Callback<ArrayList<Esport>>() {
            @Override
            public void onResponse(Call<ArrayList<Esport>> call, Response<ArrayList<Esport>> response) {
                esports = response.body();
                ArrayList<String> list = new ArrayList<>();

                for (int i = 0; i < esports.size(); i++) {
                    list.add(esports.get(i).getNom());
                }
                ArrayAdapter<String> spinnerEsports = new ArrayAdapter<String>
                        (EditarEquip.this, android.R.layout.simple_spinner_item,
                                list);
                spinnerEsports.setDropDownViewResource(R.layout.spinneritemlayout);
                esportSpinner.setAdapter(spinnerEsports);
                if (equip.getId_esport() != 0) {
                    for (int i = 0; i < esports.size(); i++) {
                        if (esports.get(i).getId() == equip.getId_esport()) {
                            esportSpinner.setSelection(i);
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ArrayList<Esport>> call, Throwable t) {
                Toast.makeText(EditarEquip.this, t.toString(), Toast.LENGTH_LONG).show();
            }

        });

        callCategoria.enqueue(new Callback<ArrayList<Categoria>>() {
            @Override
            public void onResponse(Call<ArrayList<Categoria>> call, Response<ArrayList<Categoria>> response) {
                categories = response.body();
                ArrayList<String> list = new ArrayList<>();

                for (int i = 0; i < categories.size(); i++) {
                    list.add(categories.get(i).getNom());
                }

                ArrayAdapter<String> spinnerCategories = new ArrayAdapter<String>
                        (EditarEquip.this, android.R.layout.simple_spinner_item,
                                list);
                spinnerCategories.setDropDownViewResource(R.layout.spinneritemlayout);
                categoriaSpinner.setAdapter(spinnerCategories);
                if (equip.getId_categoria() != 0) {
                    for (int i = 0; i < categories.size(); i++) {
                        if (categories.get(i).getId() == equip.getId_categoria()) {
                            categoriaSpinner.setSelection(i);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Categoria>> call, Throwable t) {
                Toast.makeText(EditarEquip.this, t.toString(), Toast.LENGTH_LONG).show();
            }

        });

        callSexe.enqueue(new Callback<ArrayList<Sexe>>() {
            @Override
            public void onResponse(Call<ArrayList<Sexe>> call, Response<ArrayList<Sexe>> response) {
                sexes = response.body();
                ArrayList<String> list = new ArrayList<>();

                for (int i = 0; i < sexes.size(); i++) {
                    list.add(sexes.get(i).getNom());
                }

                ArrayAdapter<String> spinnerSexes = new ArrayAdapter<String>
                        (EditarEquip.this, android.R.layout.simple_spinner_item,
                                list);
                spinnerSexes.setDropDownViewResource(R.layout.spinneritemlayout);
                sexeSpinner.setAdapter(spinnerSexes);
                if (equip.getId_sexe() != 0) {
                    for (int i = 0; i < sexes.size(); i++) {
                        if (sexes.get(i).getId() == equip.getId_sexe()) {
                            sexeSpinner.setSelection(i);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Sexe>> call, Throwable t) {
                Toast.makeText(EditarEquip.this, t.toString(), Toast.LENGTH_LONG).show();
            }

        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equip.setNom(nom.getText().toString());

                selectedEsport = esportSpinner.getSelectedItemPosition();
                equip.setId_esport(esports.get(selectedEsport).getId());
                // equip.setEsports(esports.get(selectedEsport));

                selectedCategoria = categoriaSpinner.getSelectedItemPosition();
                equip.setId_categoria(categories.get(selectedCategoria).getId());
                // equip.setCategories(categories.get(selectedCategoria));

                selectedSexe = sexeSpinner.getSelectedItemPosition();
                equip.setId_sexe(sexes.get(selectedSexe).getId());
                // equip.setSexes(sexes.get(selectedSexe));

                if (!creatingTeam) {
                    equip.clearRelationships();

                    Call<ResponseBody> callUpdateEquip = serviceEquip.updateEquip(equip.getId(), equip);

                    callUpdateEquip.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            String[] responseSplit = String.valueOf(response.code()).split("");
                            if (responseSplit[1].equals("2")) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                Toast.makeText(EditarEquip.this, response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(EditarEquip.this, t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                } else if (creatingTeam) {
                    equip.setId_entitat(Preferences.getUserID());
                    equip.setId_competicio(1);

                    Call<Equip> callCreateEquip = serviceEquip.newEquip(equip);

                    callCreateEquip.enqueue(new Callback<Equip>() {
                        @Override
                        public void onResponse(Call<Equip> call, Response<Equip> response) {
                            String[] responseSplit = String.valueOf(response.code()).split("");
                            if (responseSplit[1].equals("2")) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                Toast.makeText(EditarEquip.this, response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Equip> call, Throwable t) {
                            Toast.makeText(EditarEquip.this, t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private void OmplirPantalla() {
        nom = findViewById(R.id.editTextNom);
        categoriaSpinner = findViewById(R.id.comboBoxCategoria);
        sexeSpinner = findViewById(R.id.comboBoxSexe);
        esportSpinner = findViewById(R.id.comboBoxEsport);
        floatingActionButton = findViewById(R.id.floationButtonDone);

        if (equip.getNom() != null) {
            nom.setText(equip.getNom());
        }
    }
}
