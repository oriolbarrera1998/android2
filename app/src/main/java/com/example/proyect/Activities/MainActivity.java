package com.example.proyect.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyect.API.API;
import com.example.proyect.API.Services.EntitatService;
import com.example.proyect.API.Threads.EntityThread;
import com.example.proyect.Exceptions.NullEntityException;
import com.example.proyect.Fragments.ProfileFragment;
import com.example.proyect.Fragments.StartDemandFragment;
import com.example.proyect.Interfaces.ActionBar;
import com.example.proyect.POJO.Entitat;
import com.example.proyect.R;
import com.example.proyect.Utils.Preferences;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private static Entitat entitat;
    public static final String ENTITAT = "ENTITAT";
    private static boolean EntityRetrieved;
    private boolean isNewEntity = false;

    public static final int REQUEST_EDIT_ENTITAT = 1;
    public static final int REQUEST_EDIT_EQUIP = 2;
    public static final int REQUEST_DEMANDA_DETALLS = 3;
    public static final int REQUEST_VIDEO_CAPTURE = 50;

    TextView nom, correu;
    ImageView image;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Preferences.setSettings(getSharedPreferences(Preferences.getPrefsName(), 0));
        Preferences.setTheme(this);
        super.onCreate(savedInstanceState);

        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //region RETRIVE ENTITY FROM DATABASE
        //Retrieve whole entity from DB

        if (!isNewEntity) {
            if (!getEntityRetrieved()) {
                try {
                    MainActivity.entitat = getEntitatFromDB(Preferences.getUserID());
                } catch (NullEntityException ex) {
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                MainActivity.entitat = Preferences.getEntitat();
            }
        }

//        try {
//            MainActivity.entitat = getEntitatFromDB(Preferences.getUserID());
//        } catch (NullEntityException ex) {
//            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
//        }

        setEntityRetrieved(true);
        Preferences.saveEntitat(entitat);
        //endregion

        //region SEND_ENTITY_TO_PROFILE_FRAGMENT
        // Paso la entitat al Fragment amb un bundle.
        Bundle bundle = new Bundle();
        bundle.putParcelable(ENTITAT, entitat);
        // Aquestes linies fan referencia al Fragment al que li enviem les dades
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //endregion

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new ProfileFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_message);
        }

        //region THEME_SWITH
        // darkMode Switch
        Menu menu = navigationView.getMenu();
        MenuItem item = menu.findItem(R.id.dark_mode_switch);

        Switch sw = item.getActionView().findViewById(R.id.dark_mode_switch);
        if (Preferences.isDarkModeSaved()) {
            sw.setChecked(true);
        }

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Preferences.saveThemePreferences(true);


                } else {
                    Preferences.saveThemePreferences(false);

                }
                Preferences.setTheme(MainActivity.this);
                Preferences.restartActivityAfterThemeChanged(MainActivity.this);
            }
        });

		navigationView = findViewById(R.id.nav_view);
		View header = navigationView.getHeaderView(0);
		nom = header.findViewById(R.id.nomDrawer);
		correu = header.findViewById(R.id.mailDrawer);
		image = header.findViewById(R.id.ImageDrawer);
		image.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(entitat.getImatge(), "drawable", getPackageName())));
		nom.setText(entitat.getNom());
		correu.setText(entitat.getCorreu());

        //endregion

    }

    //region ON MENU ITEM CLICK
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_message:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfileFragment()).commit();
                break;
            case R.id.nav_chat:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new StartDemandFragment()).commit();
                break;
            case R.id.nav_share:
                if(entitat.getTwitter() != null){
                    Intent twitter = new Intent(Intent.ACTION_VIEW, Uri.parse(entitat.getTwitter()));
                    startActivity(twitter);
                }else{
                    Toast.makeText(MainActivity.this, "No s'ha configurat el Twitter", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.nav_send:
                if(entitat.getInstagram() != null){
                    Intent instagram = new Intent(Intent.ACTION_VIEW, Uri.parse(entitat.getInstagram()));
                    startActivity(instagram);
                }else{
                    Toast.makeText(MainActivity.this, "No s'ha configurat el Instagram", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.instagram:
                if(entitat.getFacebook() !=null){
                    Intent facebook = new Intent(Intent.ACTION_VIEW, Uri.parse(entitat.getFacebook()));
                    startActivity(facebook);
                }else{
                    Toast.makeText(MainActivity.this, "No s'ha configurat el Facebook", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.logout:
                logout();
                break;
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //endregion

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Elimina les credencials guardades del usuari, i redirigeix la app a Login.class
     *
     * @since 1.0
     */
    public void logout() {
        Preferences.logout();
        Preferences.setIsEntityRetrieved(false);
        Intent i = new Intent(MainActivity.this, Login.class);
        startActivity(i);
        finish();
    }

    /**
     * Retorna una entitat cada cop que s'entra a la app, agafant
     * el id guardat a les SharedPreference
     *
     * @param id es el id de la entitat
     * @return Entitat segons el id passat per paramtetre
     * @throws NullEntityException quan la entitat recuperada es null (no existeix)
     * @since 1.0
     */
    public static Entitat getEntitatFromDB(int id) throws NullEntityException {

        EntityThread entityThread = new EntityThread(id);
        entityThread.start();
        try {
            entityThread.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        Entitat e = entityThread.getEntitat();

        if (e == null) {
            throw new NullEntityException();
        }

        MainActivity.entitat = e;
        if(e != null){
            setEntityRetrieved(true);
        }

        return e;
    }

    public static Entitat getMyEntitat() {
        return entitat;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Preferences.setIsEntityRetrieved(false);
    }

    public static boolean getEntityRetrieved() {
        return EntityRetrieved;
    }

    public static void setEntityRetrieved(boolean entityRetrieved) {
        EntityRetrieved = entityRetrieved;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceStat) {

        super.onSaveInstanceState(savedInstanceStat);
        savedInstanceStat.putBoolean("isEntityRetrieved", MainActivity.getEntityRetrieved());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setEntityRetrieved(savedInstanceState.getBoolean("isEntityRetrieved"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case REQUEST_DEMANDA_DETALLS:
            case REQUEST_EDIT_EQUIP:
            case REQUEST_EDIT_ENTITAT:
                if(resultCode == RESULT_OK) {
                    setEntityRetrieved(false);
                    finish();
                    startActivity(getIntent());
                }
                break;
            case REQUEST_VIDEO_CAPTURE:
                if(resultCode == RESULT_OK) {
                    entitat.setVideo(data.getData().toString());
                    EntitatService service = API.getApi().create(EntitatService.class);

                    entitat.setEquips(null);
                    entitat.setTelefons(null);

                    Call<ResponseBody> callUpdate = service.updateEntitat(entitat.getId(), entitat);

                    callUpdate.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 204) {
                                Toast.makeText(MainActivity.this, "Perfil actualitzat correctament", Toast.LENGTH_SHORT).show();
                                setEntityRetrieved(false);
                                finish();
                                startActivity(getIntent());
                            } else if (response.code() == 401) {
                                Toast.makeText(MainActivity.this, "Unauthorized (HOSTING)", Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    Toast.makeText(MainActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                //Toast.makeText(EditarEntitat.this, "Alguna cosa ha anat malament...", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            // Toast.makeText(EditarEntitat.this, t.toString(), Toast.LENGTH_LONG).show();
                            Toast.makeText(MainActivity.this, "No s'ha pogut connectar amb el servidor", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
        }


    }

    public static void setEntitat(Entitat e){
        MainActivity.entitat = e;
    }

    public static Entitat getEntitat(){
        return MainActivity.entitat;
    }
}