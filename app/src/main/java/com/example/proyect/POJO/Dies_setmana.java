package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Dies_setmana implements Parcelable {
    private int id;
    private String dia;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.dia);
    }

    public Dies_setmana() {
    }

    public Dies_setmana(String dia) {
        this.dia = dia;
    }

    protected Dies_setmana(Parcel in) {
        this.id = in.readInt();
        this.dia = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public static final Creator<Dies_setmana> CREATOR = new Creator<Dies_setmana>() {
        @Override
        public Dies_setmana createFromParcel(Parcel source) {
            return new Dies_setmana(source);
        }

        @Override
        public Dies_setmana[] newArray(int size) {
            return new Dies_setmana[size];
        }
    };
}
