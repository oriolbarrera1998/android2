package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Telefon implements Parcelable {
    private Integer id;
    private int id_entitat;
    private String telefon;
    private String rao;

    public int getId_entitat() {
        return id_entitat;
    }

    public void setId_entitat(int id_entitat) {
        this.id_entitat = id_entitat;
    }

    public Integer getId() {
        return id;
    }
    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getRao() {
        return rao;
    }

    public void setRao(String rao) {
        this.rao = rao;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeInt(this.id_entitat);
        dest.writeString(this.telefon);
        dest.writeString(this.rao);
    }

    protected Telefon(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id_entitat = in.readInt();
        this.telefon = in.readString();
        this.rao = in.readString();
    }

    public static final Creator<Telefon> CREATOR = new Creator<Telefon>() {
        @Override
        public Telefon createFromParcel(Parcel source) {
            return new Telefon(source);
        }

        @Override
        public Telefon[] newArray(int size) {
            return new Telefon[size];
        }
    };
}
