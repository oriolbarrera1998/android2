package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Espai implements Parcelable {
    private int id;
    private int id_instalacio;
    private String nom;
    private double preu;
    private String imatge;
    private boolean es_exterior;
    private boolean borrat;
    private Instalacio instalacions;

    public Instalacio getInstalacions() {
        return instalacions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPreu() {
        return preu;
    }

    public String getImatge() {
        return imatge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.id_instalacio);
        dest.writeString(this.nom);
        dest.writeDouble(this.preu);
        dest.writeString(this.imatge);
        dest.writeByte(this.es_exterior ? (byte) 1 : (byte) 0);
        dest.writeByte(this.borrat ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.instalacions, flags);
    }

    public Espai() {
    }

    protected Espai(Parcel in) {
        this.id = in.readInt();
        this.id_instalacio = in.readInt();
        this.nom = in.readString();
        this.preu = in.readDouble();
        this.imatge = in.readString();
        this.es_exterior = in.readByte() != 0;
        this.borrat = in.readByte() != 0;
        this.instalacions = in.readParcelable(Instalacio.class.getClassLoader());
    }

    public static final Creator<Espai> CREATOR = new Creator<Espai>() {
        @Override
        public Espai createFromParcel(Parcel source) {
            return new Espai(source);
        }

        @Override
        public Espai[] newArray(int size) {
            return new Espai[size];
        }
    };
}