package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Esport implements Parcelable {
    private int id;
    private String nom;

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
    }

    protected Esport(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
    }

    public static final Creator<Esport> CREATOR = new Creator<Esport>() {
        @Override
        public Esport createFromParcel(Parcel source) {
            return new Esport(source);
        }

        @Override
        public Esport[] newArray(int size) {
            return new Esport[size];
        }
    };
}
