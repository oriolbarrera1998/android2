package com.example.proyect.POJO;

import android.os.Parcel;
import android.os.Parcelable;

public class Sexe implements Parcelable {
    private int id;
    private String nom;

    public Sexe(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nom);
    }

    protected Sexe(Parcel in) {
        this.id = in.readInt();
        this.nom = in.readString();
    }

    public static final Creator<Sexe> CREATOR = new Creator<Sexe>() {
        @Override
        public Sexe createFromParcel(Parcel source) {
            return new Sexe(source);
        }

        @Override
        public Sexe[] newArray(int size) {
            return new Sexe[size];
        }
    };
}
